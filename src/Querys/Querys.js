import { gql } from "@apollo/client";

export const MAIN_QUERY = gql`
query MainQuery{
  master_categories {
    key
    name
    active
    icon
  }
}
`
export const CATEGORY_QUERY = gql`
query CategoryQuery($category: uuid = "" ,$isActive: Boolean ) {
  categories(where: {_or: [
    {master_category_id: {_eq: $category}},
    {active: {_eq: $isActive}}
  ]}) {
    active
    icon
    key
    name
    rdmValue
    master_category_id
  }
}
`

export const SUBATEGORY_QUERY = gql`
query SubCategoryQuery($subCategory: uuid) {
  services(where: {category_id: {_eq: $subCategory}}) {
    category_id
    is_bundle
    key
    name
    online
    price
    review
    selected
    time
  }
}
`
export const ADD_QUERY = gql`
mutation insertService(
  $category: uuid , 
  $name: String , 
  $time: String, 
  $price: float8, 
  $online: Boolean, 
  $selected: Boolean ,
  $review: Int ,
  $is_bundle: Boolean ,
) {
  insert_services_one(object: {
    category_id: $category, 
    name: $name, 
    time: $time, 
    price: $price, 
    online: $online, 
    selected: $selected,
    review:$review,
    is_bundle:$is_bundle
  }) {
    name
    category_id
    time
    online
    selected
    price
    review
  }
}
`

export const DELETE_ITEM = gql`
mutation deleteService($serviceID: uuid!) {
  delete_services_by_pk(key: $serviceID) {
    key
  }
}
`