import { useMutation, useQuery } from "@apollo/client";
import { Input, Form, Select, Button } from "antd";
import { useState } from "react/cjs/react.development";
import styled from "styled-components";
import ContentData from "../../ContentData/ContentData";
import { ADD_QUERY, CATEGORY_QUERY, MAIN_QUERY, SUBATEGORY_QUERY } from "../../Querys/Querys";

const FormWrapper = styled(Form)`
    margin-top:30px;
    .checkbox_label{
        color:#d9d9d9;
        margin-left:10px;
        text-transform: capitalize;
    }
    .ant-form-item-control-input-content{
        display:flex;
        align-items:center;
        animation: 0.5s ease 0s 1 normal none running fadeIn;
        @keyframes fadeIn {
            0% { opacity: 0;}
            80% { opacity: 0.5;}
            100% { opacity: 1;}
        }
    }
    .check_item{
        display: inline-block;
        width: calc(24% - 4px);
        box-sizing:border-box;
        margin-left: 8.33333333%;

    }
`

const layout = {
    wrapperCol: {
        span: 20,
        offset: 2
    },
};

const FormBox = ({ handlePopUp }) => {
    const [allData, setAllData] = useState({
        mainCategory: "",
        category: "",
        name: "",
        time: "",
        price: "",
        online: false,
        selected: false,
        review: "",
        is_bundle: false
    })

    const { data: mainCategory } = useQuery(MAIN_QUERY)

    const { data: subCategory } = useQuery(CATEGORY_QUERY, {
        variables: { category: allData.mainCategory, isActive: true },
        broadcast:true
    });

    const [addService] = useMutation(ADD_QUERY);
  
    const handleSubmit = () => {
        handlePopUp()
        addService({
            variables: {
                category: allData.category,
                name: allData.name,
                time: allData.time,
                price: allData.price,
                online: allData.online,
                selected: allData.selected,
                review: allData.review,
                is_bundle: allData.is_bundle
            },
            update: (cache) => {
                const data = cache.readQuery({ query: SUBATEGORY_QUERY, variables: { subCategory: allData.category } });
                if (data != undefined) {
                    cache.writeQuery({
                        query: SUBATEGORY_QUERY, variables: { subCategory: allData.category }, data: {
                            services: [...data.services, addService],
                        },
                    });
                }
            }
        })
    }


    return (
        <FormWrapper
            {...layout}
            autoComplete="off"
            onFinish={handleSubmit}
        >
            <Form.Item
                name="Main Category"
                rules={[
                    {
                        required: true,
                    },
                ]}>
                <Select onChange={(e) => setAllData({ ...allData, mainCategory: e })} placeholder={ContentData.Form.master}>
                    {mainCategory && mainCategory.master_categories.map((el, index) => (
                        <Select.Option value={el.key} key={index}>{el.name}</Select.Option>
                    ))}
                </Select>
            </Form.Item>
            <Form.Item
                name="Category"
                rules={[
                    {
                        required: true,
                    },
                ]}>
                <Select placeholder={ContentData.Form.subcategory} onChange={(e) => setAllData({ ...allData, category: e })}>
                    {subCategory && subCategory.categories.map((el, index) => (
                        <Select.Option value={el.key} key={index}>{el.name}</Select.Option>
                    ))}
                </Select>
            </Form.Item>
            {allData.category && ContentData.Form.inputs.map((el, index) => (
                el.name &&
                <Form.Item
                    key={index}
                    className={el.type === "checkbox" && "check_item"}
                >
                    <Input placeholder={el.placeholder} type={el.type} name={el.name} value={allData[el.name]}
                        onChange={(e) => setAllData({ ...allData, [e.target.name]: e.target.type === 'checkbox' ? e.target.checked : e.target.value })}
                        className={el.type === "checkbox" && "check"} />
                    {el.type === "checkbox" && <span className="checkbox_label">{el.name}</span>}
                </Form.Item>
            ))
            }
            <Form.Item >
                <Button type="primary" htmlType="submit" >{ContentData.Form.sendButton}</Button>
            </Form.Item>
        </FormWrapper >
    );
}

export default FormBox;