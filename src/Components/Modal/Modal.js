import { Button, Modal } from 'antd';

const ModalWrapper = ({ title, buttonText,popUp,handlePopUp,children}) => {
  
    return (
        <Modal title={title} visible={popUp} onCancel={handlePopUp}
            footer={[
                <Button key="back" onClick={handlePopUp} type="primary">
                    {buttonText}
                </Button>
            ]}
        >
         {children}
        </Modal>
    );
}

export default ModalWrapper;