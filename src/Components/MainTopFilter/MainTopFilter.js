import { BoxStyle } from '../Style/Style';
import { Col, Row } from 'antd';
import TopCard from '../TopCard/TopCard';


const MainTopFilter = ({ setCategory ,setIsActive,active,setActive ,data}) => {
    return (
        <BoxStyle>
            <Row>
                <Col span={3} >
                    <TopCard
                        title="All"
                        icon="all"
                        id="29ffd6d0-72d5-4d92-9030-48c246c128db"
                        setCategory={setCategory}
                        setIsActive={setIsActive}
                        setActive={setActive}
                        active={active}
                    />
                </Col>
                {data && data.master_categories.map((el, index) => (
                    <Col span={3} key={index}>
                        <TopCard
                            title={el.name}
                            icon={el.icon}
                            id={el.key}
                            setCategory={setCategory}
                            setIsActive={setIsActive}
                            setActive={setActive}
                            active={active}
                            />
                    </Col>
                ))
                }
                <Col span={3}>
                    <TopCard
                        title="Voucher"
                        icon="voucher"
                        setCategory={setCategory}
                        setIsActive={setIsActive}
                        setActive={setActive}
                        active={active}
                    />
                </Col>
            </Row>
        </BoxStyle>
    );
}

export default MainTopFilter;