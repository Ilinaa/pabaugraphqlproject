import { useState } from 'react';
import styled from 'styled-components';
import MainTopFilter from '../MainTopFilter/MainTopFilter';
import { Row, Col, Button } from 'antd';
import { BoxStyle } from '../Style/Style';
import SubFilter from '../SubFilter/SubFilter';
import { List } from 'antd';
import ContentData from '../../ContentData/ContentData';
import SubFilterData from '../SubFilterData/SubFilterData';
import { ArrowRightOutlined, PlusCircleFilled } from '@ant-design/icons';
import ModalWrapper from '../Modal/Modal';
import usePopUp from '../Hooks/usePopUp';
import FormBox from '../Form/FormBox';
import { useQuery } from '@apollo/client';
import { MAIN_QUERY } from '../../Querys/Querys';

const ContentWrapper = styled.div`
  background: #f7f7f9;
  padding: 50px 0px;
  .addService{
    margin-bottom:20px;
    text-align: right;
    button{
      margin-right: 16px;
    }
  }
  .content_bottom{
    margin: 32px 0px 100px;
  }
  .fixed_top{
    width: 100%;
    position: fixed !important;
    bottom: 45px;
    .price_popUp{
      border-radius: 4px 4px 0px 0px;
      display: flex !important;
      align-items: center !important;
      background: #fff;
      margin-right: 16px !important;
      justify-content: space-between !important;
      padding: 16px 20px;
      box-shadow: 0 0 3px 0 rgba(0,0,0,.2784313725490196),0 0 0 1px rgba(0,0,0,.0784313725490196)!important;
      p, .button{
        text-align: right !important;
        margin: 0px;

      }
      p{
        color: #54b2d3;
        font-size: 20px;
        font-weight: 500;
    
      }

    }
  }
`

const Content = () => {
  const [category, setCategory] = useState("29ffd6d0-72d5-4d92-9030-48c246c128db")
  const [subCategory, setSubCategory] = useState("2e1dd471-1fde-4906-81bf-251385b00365")
  const [status, setStatus] = useState("In Clinic")
  const [isActive, setIsActive] = useState(false)
  const [isSelectedList, setisSelectedList] = useState([]);
  const [showPrice, setshowPrice] = useState(0)
  const [active, setActive] = useState({ mainCategory: "All" })
  const [popUp, handlePopUp] = usePopUp();

  const { data:mainCategory } = useQuery(MAIN_QUERY)
  

  const handleSelected = (name, price) => {
    let actualPrice = parseInt(price);
    let selectedItems = [...isSelectedList];

    // Checks if the name is in the list, if it is, then finds it and remove it, or add it to list

    if (selectedItems.includes(name)) {
      let index = selectedItems.indexOf(name)
      if (index !== -1) {
        selectedItems.splice(index, 1)
        setshowPrice(showPrice - actualPrice)
      }
    } else {
      selectedItems.push(name)
      setshowPrice(actualPrice + showPrice)
    }
    setisSelectedList(selectedItems)
  }


  return (

    <ContentWrapper>
      <Row>
        <Col span={12} offset={6} className="addService">
          <Button type="primary" onClick={() => handlePopUp()}>{ContentData.ModalAddService.button} <PlusCircleFilled /></Button>
          <ModalWrapper popUp={popUp} handlePopUp={handlePopUp} buttonText={"Close"}>
            <FormBox handlePopUp={handlePopUp}/>
          </ModalWrapper>
        </Col>
      </Row>
      <Row>
        <Col span={12} offset={6}>
          <MainTopFilter
            setCategory={setCategory}
            setIsActive={setIsActive}
            active={active}
            setActive={setActive}
            data={mainCategory}
          />
        </Col>
      </Row>
      <Row className="content_bottom">
        <Col span={6} offset={6} >
          <BoxStyle>
            <SubFilter
              category={category}
              setSubCategory={setSubCategory}
              isActive={isActive}
              setIsActive={setIsActive}
              subCategory={subCategory}
            />
          </BoxStyle>
        </Col>
        <Col span={6}>
          <BoxStyle>
            <List
              dataSource={ContentData.SubCategoryChoices}
              className="box_list"
              renderItem={item => (
                <List.Item className={`list_style ${status === item.name && "active"}`} onClick={() => setStatus(item.name)}>
                  <span><item.icon /></span> {item.name}
                </List.Item>
              )}
            />
            <SubFilterData
              subCategory={subCategory}
              status={status}
              handleSelected={(name, price) => handleSelected(name, price)}
              isSelectedList={isSelectedList}
            />
          </BoxStyle>
          <Row>
            <Col span={24}>
              <BoxStyle marginTop>
                <p>{ContentData.Contact.text} <span className="phone">{ContentData.Contact.phone}</span></p>
              </BoxStyle>
            </Col>
          </Row>
        </Col>
      </Row>
      {showPrice > 0 &&
        <Row className="fixed_top">
          <Col span={12} offset={6}>
            <Row className="price_popUp">
              <Col span={14}>
                <p> {isSelectedList.length} {ContentData.Selected.span} &#163; {showPrice}</p>
              </Col>
              <Col span={10} className="button">
                <Button type="primary">{ContentData.Selected.button} <ArrowRightOutlined /></Button>
              </Col>
            </Row>
          </Col>
        </Row>
      }
    </ContentWrapper>
  );
}

export default Content;