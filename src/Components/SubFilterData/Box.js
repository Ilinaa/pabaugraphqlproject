import { Alert, Button, Card } from 'antd';
import styled from 'styled-components';
import { CheckCircleOutlined, ClockCircleOutlined, QuestionCircleOutlined, StarFilled } from '@ant-design/icons';
import usePopUp from '../Hooks/usePopUp';
import ContentData from '../../ContentData/ContentData';
import ModalWrapper from '../Modal/Modal';
import { DELETE_ITEM, SUBATEGORY_QUERY } from '../../Querys/Querys';
import { useMutation } from '@apollo/client';


const CardWrapper = styled(Card)`
    width: 100%;
    padding: 16px !important;
    border-radius: 4px;
    margin: 16px 0px!important;
    .box_middle,.box_bottom{
        text-align: left !important;
        padding: 7px 0px;
    }
    .box_top{
        display: flex;
        justify-content: space-between;
    }
    .box_top .box_span_name{
        font-size: 20px;
        line-height: 24px;
        color: #3d3d46;
        margin-right: 5px;
        text-transform: capitalize;
    }
    .box_top .box_span_right{
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        color: #65cd98;
    }
    .box_span_reviews{
        margin-left: 10px;
        color: #737387;
        font-size: 16px;
    }
    .box_middle span{
        color: #737387;
        font-size: 16px;
        line-height: 20px;
    }
    .box_selected{
        text-align: right;
    }
    .box_selected button{
        background-color: #54b2d3 !important;
        border-radius: 4px !important;
        color: #fff !important;
        font-size: 14px;
        font-weight: 700;
    }

`
const Selected = ({ isSelectedList, button, name, handlePopUp }) => {
    return (
        isSelectedList.map((el, i) => (
            el === name ?
                button ?
                    <Button className="selected_button" icon={<CheckCircleOutlined key={i} />}>{ContentData.Selected.box_button} </Button> :
                    <Alert
                        banner
                        message={ContentData.SubFilterData.selectedMessage}
                        key={i}
                        type="info"
                        action={
                            <Button size="small" type="text" onClick={() => handlePopUp()}>
                                {ContentData.SubFilterData.moreInfo}
                            </Button>
                        } />
                : null
        ))
    )
}

const Box = ({ name, price, review, time, handleSelected, isSelectedList, id, subCategory }) => {
    const [popUp, handlePopUp] = usePopUp();

    const [deleteItem] = useMutation(DELETE_ITEM);

    const handleDelete = (id, subCategory) => {

        deleteItem({
            variables: {
                serviceID: id
            },
            update: (cache) => {
                const data = cache.readQuery({ query: SUBATEGORY_QUERY, variables: { subCategory: subCategory } });
                let dataD = { ...data }
                dataD.services = data.services.filter(({ key }) => key !== id);
                cache.writeQuery({
                    query: SUBATEGORY_QUERY, variables: { subCategory: subCategory }, data: {
                        services: dataD,
                    },
                });
                        }
        })

    }

    return (
        <>
            <CardWrapper
                hoverable
                name={name}
                onClick={() => handleSelected(name, price)}
            >
                <div className="box_top">
                    <div>
                        <span className="box_span_name">{name} </span>
                        <span><QuestionCircleOutlined /></span>
                    </div>
                    <span className="box_span_right">&#163; {price}</span>
                </div>
                <div className="box_middle">
                    <span>{time} min <span><ClockCircleOutlined /></span></span>
                </div>
                <div className="box_bottom">
                    {([...Array(5)].map((x, i) => <StarFilled key={i} style={{ color: "#fadb14", fontSize: "20px" }} />))}
                    <span className="box_span_reviews">{review} reviews</span>
                </div>
                <div className="box_selected">
                    <Selected button handlePopUp={handlePopUp} name={name} isSelectedList={isSelectedList} />
                </div>
            </CardWrapper>
            <Selected isSelectedList={isSelectedList} handlePopUp={handlePopUp} name={name} />
            <ModalWrapper
                title={ContentData.MoreInfoModal.heading}
                popUp={popUp}
                handlePopUp={handlePopUp}
                buttonText={ContentData.MoreInfoModal.button}
            >
                <p>{ContentData.MoreInfoModal.text}</p>
            </ModalWrapper>
            <Button type='primary' onClick={() => handleDelete(id, subCategory)}>Delete</Button>
        </>
    );
}

export default Box;