import { useQuery } from "@apollo/client";
import { SUBATEGORY_QUERY } from "../../Querys/Querys";
import Box from './Box';

const SubFilterData = ({ subCategory , status, handleSelected, isSelectedList }) => {

    const { data } = useQuery(SUBATEGORY_QUERY, {
        variables: { subCategory },
    });


    return (
        <>
            {data && data.services.map((service, index) => (
                <div key={index}>
                    {(status === "In Clinic" && !service.online) ?
                        <Box
                            name={service.name}
                            price={service.price}
                            time={service.time}
                            review={service.review}
                            handleSelected={handleSelected}
                            id={service.key}
                            isSelectedList={isSelectedList}
                            subCategory={subCategory}
                        />
                        : (status === "Virtual Consultation" && service.online) ?
                            <Box
                                name={service.name}
                                key={service.key}
                                price={service.price}
                                time={service.time}
                                review={service.review}
                                handleSelected={handleSelected}
                                isSelectedList={isSelectedList}
                                subCategory={subCategory}
                            />
                            : null
                    }
                </div>
            ))
            }
        </>);
}

export default SubFilterData;