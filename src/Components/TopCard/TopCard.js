// import { Card } from 'antd';
// import Meta from 'antd/lib/card/Meta';
// import React from 'react';
// import './TopCard.css';

// const TopCard = ({icon,title,handleFilter,active,setActive,selectedSubCategory}) => {

//     return ( 
//         <Card
//           hoverable
//           cover={
//             <img
//               alt={icon}
//               src={require(`../../assets/${icon.toLowerCase()}.svg`).default}
//             />
//           }
//           className={`top_card ${active && active.mainCategory === title && "active"}`}
//           onClick={()=> {handleFilter(title); setActive({ mainCategory: title, category: selectedSubCategory && selectedSubCategory, subCategory: "In Clinic" })}}
//         >
//         <Meta title={title}/>
//        </Card>
//      );
// }

// export default TopCard;


import { Card } from 'antd';
import Meta from 'antd/lib/card/Meta';
import React from 'react';
import './TopCard.css';

const TopCard = ({ icon, title, id, setCategory, setIsActive ,active,setActive}) => {

  return (
    <Card
      hoverable
      cover={
        <img
          alt={icon}
          src={require(`../../assets/${icon.toLowerCase()}.svg`).default}
        />
      }
      className={`top_card ${active && active.mainCategory === title && "active"}`}
      onClick={() => { setCategory(id); title === "All" ? setIsActive(false) : setIsActive(true);setActive({mainCategory: title})}}
    >
      <Meta title={title} />
    </Card>
  );
}

export default TopCard;