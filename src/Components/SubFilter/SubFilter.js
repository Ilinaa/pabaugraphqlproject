import { useQuery } from "@apollo/client";
import { CATEGORY_QUERY } from "../../Querys/Querys";
import { List } from 'antd';
import { useEffect } from "react/cjs/react.development";

const SubFilter = ({ category, setSubCategory,isActive,subCategory }) => {

    const { data } = useQuery(CATEGORY_QUERY, {
        variables: { category, isActive }
    });

    useEffect(() => {
        data && setSubCategory(data.categories[0].key)
    }, [data])


    return (
        <List
            size="medium"
            dataSource={data && data.categories}
            renderItem={item =>
                <List.Item key={item.name}
                    className={`list_style ${subCategory === item.key && "active"}`}
                    onClick={() => setSubCategory(item.key)}
                >
                    {item.name} <span>{item.rdmValue}</span>

                </List.Item>}
        />
    )

}

export default SubFilter;