import { MedicineBoxOutlined, LaptopOutlined } from '@ant-design/icons';
let ContentData = {
    Header: {
        text: "Choose Service",
        span: "Step 1/8",
    },
    SubCategoryChoices: [{ name: "In Clinic", icon: MedicineBoxOutlined }, { name: "Virtual Consultation", icon: LaptopOutlined }],
    SubFilterData: {
        selectedMessage: "You may need a patch test",
        moreInfo: "more info"
    },
    MoreInfoModal: {
        heading: "Patch test",
        text: "To make sure your skin doesen’t react to the products used in your treatment, please book a patch test for at least 48 hours before your appointment.To make sure your skin doesen’t react to the products used in your treatment, please book a patch test for at least 48 hours before your appointment.",
        button: "I understand"
    },
    Selected: {
        box_button: "Selected",
        span: "services",
        button: "Next"
    },
    Contact: {
        text: "Not sure about consultation type? Please, call",
        phone: "045787498450"
    },
    ModalAddService: {
        button: "Add service"
    },
    Form: {
        master: "Select main category",
        subcategory: "Select a sub category",
        sendButton: "Add",
        inputs: [
            {
                name: "name",
                placeholder: "Enter the name of the service",
                type: "text"
            },
            {
                name: "time",
                placeholder: "Enter the time",
                type: "number"
            },
            {
                name: "price",
                placeholder: "Enter the price",
                type: "number"
            },
            {
                name: "online",
                placeholder: "Enter if is online or not",
                type: "checkbox",
            },
            {
                name: "selected",
                placeholder: "Enter if is selected",
                type: "checkbox"
            },
            {
                name: "is_bundle",
                placeholder: "Is it bundle?",
                type: "checkbox"
            },
            {
                name: "review",
                placeholder: "How many reviews ,ther ar?",
                type: "number"
            },

        ]
    },
    Footer: {
        text: "Powered By Pabau"
    }


}

export default ContentData
